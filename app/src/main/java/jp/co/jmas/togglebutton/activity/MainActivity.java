package jp.co.jmas.togglebutton.activity;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

import jp.co.jmas.togglebutton.R;
import jp.co.jmas.togglebutton.fragment.FirstFragment;
import jp.co.jmas.togglebutton.fragment.SecondFragment;

public class MainActivity extends AppCompatActivity {

    Button bottom_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FirstFragment firstFragment = new FirstFragment();
        loadFragment(firstFragment,"firstFragment");


        bottom_button = (Button) findViewById(R.id.bottom_button);


        bottom_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                switch(view.getId()){
                    case R.id.bottom_button:
                        SecondFragment secondFragment = new SecondFragment();
                        loadFragment(secondFragment, "secondFragment");
                        break;
                    
                    default:
                        break;
                }
            }
        });

    }

    public void loadFragment(Fragment frag, String tag)
    {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        Fragment fragment = getFragmentManager().findFragmentById(R.id.fragment_container);

        if (fragment == null)
        {
            ft.add(R.id.fragment_container,frag,tag);
        }
        else
        {

            ft.setCustomAnimations(R.animator.slide_in,0,0,R.animator.slide_out);
            ft.replace(R.id.fragment_container,frag,tag);
            bottom_button.setEnabled(false);
        }

        ft.addToBackStack(null);
        ft.commit();
    }
}
