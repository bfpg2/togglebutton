package jp.co.jmas.togglebutton.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ToggleButton;

import jp.co.jmas.togglebutton.R;

/**
 * Created by Dell on 8/24/2016.
 */
public class FirstFragment extends Fragment {


    private static ToggleButton toggleButton;
    private Activity mActivity;
    private final String TAG = "jp.co.jmas.togglebutton.fragment.FirstFragment";

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {

            super.onAttach(activity);
            this.mActivity = activity;

        }catch (ClassCastException e){
            throw new ClassCastException(activity.toString());
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.first_fragment_layout,container,false);

        toggleButton = (ToggleButton)view.findViewById(R.id.toggleButton1);
        toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {

            }
        });

        return view;
    }
}
