package jp.co.jmas.togglebutton.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import jp.co.jmas.togglebutton.R;

/**
 * Created by Dell on 8/25/2016.
 */
public class SecondFragment extends Fragment {

    private Activity sActivity;
    private final String TAG = "jp.co.jmas.togglebutton.fragment.SecondFragment";
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.second_fragment,container,false);
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.sActivity = activity;
    }
}
